#include <utility>

#ifndef THREAD_EXECUTOR_H
#define THREAD_EXECUTOR_H


#include <sys/param.h>
#include <vector>
#include <thread>
#include <functional>
#include "Stack.h"
#include <mutex>
#include <shared_mutex>
#include <unistd.h>

using namespace std;

class ThreadExecutor {
private:
    mutable bool execute = false;
    const u_int8_t THREAD_POOL_SIZE = 8;
    const unsigned int SLEEP_DELAY = 1;

    Stack<function<void()>> stack;
    mutable shared_mutex mutex;

    int readFromQueue() const;

    function<void()> pickupFromQueue();

    bool isExecute();

    void threadListener();

    void initThreadPool();

    int getStackSize() const;

public:
    ThreadExecutor() {
        initThreadPool();
    }

    template<std::size_t size>
    explicit ThreadExecutor(function<void()> (&tasks)[size]) {
        for (std::size_t i1 = 0; i1 < size; i1++) {
            addToExecute(tasks[i1]);
        }
        initThreadPool();
    }

    ~ThreadExecutor() {
        endAllThread();
    }

    void endAllThread();

    void addToExecute(function<void()> task);
};


#endif
