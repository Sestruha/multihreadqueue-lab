#include <iostream>
#include <thread>
#include "ThreadExecutor.h"
#include <random>

void task() {
    long double firstValue = 90;
    long double lastValue = 70;
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_real_distribution<double> dist(1.0, 100000000.0);

    for (int i1 = 0; i1 < 10000000; i1++) {
        firstValue = (firstValue * dist(mt)) - (lastValue + dist(mt));
    }
}

#define TASKS_SIZE 50

int main() {
    int tasksSize = TASKS_SIZE;

    std::function<void()> tasks[TASKS_SIZE];
    for(int i1=0;i1<tasksSize;i1++){
        tasks[i1] = task;
    }

    ThreadExecutor threadExecutor(tasks);

    //Lock because ThreadExecutor implies continuous execution
    while (true){
        sleep(1);
    }
    return 0;
}