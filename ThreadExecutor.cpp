#include "ThreadExecutor.h"

int ThreadExecutor::readFromQueue() const {
    shared_lock<shared_mutex> lock(mutex);
    return stack.isEmpty() ? 0 : 1;
}

function<void()> ThreadExecutor::pickupFromQueue() {
    unique_lock<shared_mutex> lock(mutex);
    return stack.pop();
}

bool ThreadExecutor::isExecute() {
    shared_lock<shared_mutex> lock(mutex);
    return execute;
}

void ThreadExecutor::threadListener() {
    while (isExecute()) {
        if (readFromQueue()) {
            function<void()> function = pickupFromQueue();
            if (function != nullptr) {
                cout << "Thread with id:" << this_thread::get_id() << "; Started execute task ; Stack size:"
                     << getStackSize()
                     << endl;
                function();
                cout << "End execute task from thread with id:" << this_thread::get_id() << endl;
            }
        }
        sleep(SLEEP_DELAY);
    }
    cout << "End thread with id:" << this_thread::get_id() << endl;
}

void ThreadExecutor::initThreadPool() {
    unique_lock<shared_mutex> lock(mutex);
    execute = true;
    for (int i1 = 0; i1 < THREAD_POOL_SIZE; i1++) {
        thread th(&ThreadExecutor::threadListener, this);
        th.detach();
    }
}

int ThreadExecutor::getStackSize() const {
    shared_lock<shared_mutex> lock(mutex);
    return stack.getSize();
}

void ThreadExecutor::endAllThread() {
    unique_lock<shared_mutex> lock(mutex);
    execute = false;
}

void ThreadExecutor::addToExecute(function<void()> task) {
    unique_lock<shared_mutex> lock(mutex);
    stack.push(std::move(task));
}
