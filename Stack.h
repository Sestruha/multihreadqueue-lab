#ifndef STACK_H
#define STACK_H

#include <iostream>

/**
 * Queue view as Stack data structure.
 * Current realization not concurrency safe, all mechanism by multithreading in Thread Executor class.
 */
template<typename T>
class Stack {
private:

    int size;
    T *data;

    void clearData() {
        if (data) {
            delete[] data;
            data = nullptr;
        }
    }

    void copyArray(T *src, T *dest, int size) {
        for (int i1 = 0; i1 < size; i1++) {
            dest[i1] = src[i1];
        }
    }

public:

    Stack() {
        size = 0;
        data = nullptr;
    }

    ~Stack() {
        size = 0;
        clearData();
    }

    T pop() {
        if (isEmpty() || !data) {
            return nullptr;
        }

        size--;
        T returnedValue = data[size];
        T buffer[size];

        copyArray(data, buffer, size);
        clearData();

        data = new T[size];

        copyArray(buffer, data, size);

        return returnedValue;
    }

    T peek() {
        if (!isEmpty()) {
            return data[size - 1];
        } else {
            return nullptr;
        }
    }

    void push(T elem) {
        if (isEmpty()) {
            size = 1;
            data = new T[size];
            data[0] = elem;
            return;
        }

        T buffer[size];

        copyArray(data, buffer, size);
        clearData();

        size++;
        data = new T[size];

        copyArray(buffer, data, size - 1);

        data[size - 1] = elem;
    }

    void replace(T elem) {
        if (!isEmpty()) {
            data[size - 1] = elem;
        }
    }

    bool isEmpty() const {
        return size <= 0;
    }

    int getSize() const {
        return size;
    }
};

#endif
